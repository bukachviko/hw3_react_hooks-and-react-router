import React, {useState} from 'react';
import Button from "../../Button/Button";
import "./Cartpage.css";
import Modal from "../../Modal/Modal";


const Cartpage = (props) => {

    const [isShowModal, setIsShowModal] = useState(false );
    const [currentProductIdFromCart, setCurrentProductIdFromCart] = useState(0);

    const addCurrentProductFromCart = (id) => {
        setCurrentProductIdFromCart(id)
    }


    const toggleModal = () => {
        setIsShowModal(!isShowModal)
    }

    return (
        <>
        <div className="cart-items">
            <h2 className="cart-items-header">Товари додані у корзину:</h2>

            {props.items?.length === 0 && (
                <div className="cart-items-empty">Ви ще не додали жодного товара у корзину</div>
            )}

            <div>
                {props.items.map((item, key) => (
                    <div key={key} className="cart-item">
                        <Button
                            btnClass="cart-items-delete-btn"
                            text={'Х'}
                            myClick={() => {
                                toggleModal()
                                addCurrentProductFromCart(item.id)
                            }
                            }

                        />
                        <img className="cart-items-img" src={item.image} alt=""/>
                        <h3 className="cart-items-name">{item.name}</h3>
                        <p className="cart-items-cost"> ${item.cost}</p>
                    </div>
                ))}
            </div>
        </div>

            {isShowModal &&  <Modal
                btnClass={"del__item-cart-bnt"}
                backgroundColor={{backgroundColor: '#833ab4'}}
                header={"Видалити товар з кошика?"}
                text={"Підтвердіть видалення товару з кошика"}
                boolean={true}
                nameBtn1={'Так!'}
                nameBtn2={'Ні!'}
                functionSuccess={() => {
                    toggleModal()
                    props.deleteFromCart(currentProductIdFromCart)
                }}
                toggleModal={toggleModal}
            />}
        </>
    );
};

export {Cartpage};