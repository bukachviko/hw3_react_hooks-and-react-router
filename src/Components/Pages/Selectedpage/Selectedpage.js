import React from 'react';
import "./Selectedpage.css";
import {HiOutlineStar, HiStar} from "react-icons/hi2";

const Selectedpage = (props) => {
    return (
        <div className="selected-items">
            <h2 className="selected-items-header">Товари додані до обраного:</h2>

            {props.items?.length === 0 && (
                <div className="selected-items-empty">Ви ще не додали жодного товара до обраного</div>
            )}

            <div>
                {props.items.map((item, key) => (

                        <div key={key} className="selected-item">
                        <img className="selected-items-img" src={item.image} alt=""/>
                        <h3 className="selected-items-name">{item.name}</h3>

                        <div className="actions__selected"
                             onClick={() => {
                                 console.log(item)
                                 props.addToSelected({id: item.id})
                             }
                             }>
                            <HiStar fill='rgbargba(131,58,180,1)'/>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export {Selectedpage};