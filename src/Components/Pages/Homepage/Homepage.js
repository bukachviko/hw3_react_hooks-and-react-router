import React from 'react';
import GetProducts from "../../API/GetProducts";

const Homepage = (props) => {
    return <GetProducts
        setIsShowModal={props.setIsShowModal}
        items={props.items}
        initItems={props.initItems}
        addToSelected={props.addToSelected}
        isSelectedItem={props.isSelectedItem}
        addCurrentProductToCart={props.addCurrentProductToCart}
    />
}

export {Homepage};