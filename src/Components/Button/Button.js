import React from "react";
import PropTypes from "prop-types";
const Button = ({btnClass, backgroundColor, text, myClick}) => {
    return (
        <button
            className={btnClass}
            onClick= {myClick}
            style={{backgroundColor}}
        >
            {text}
        </button>
    )
}

Button.propTypes = {
    btnClass: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    myClick: PropTypes.func
}

Button.defaultProps = {
    backgroundColor: "white",
}
export default Button;