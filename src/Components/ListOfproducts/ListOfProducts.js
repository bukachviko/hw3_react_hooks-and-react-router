import ProductCard from "../ProductCard/ProductCard";
function ListOfProducts(props) {
    return (
        <ul className="card__container">
            {props.items.map(item => (
                <li
                    className="card"
                    key={item.id}>
                    <ProductCard
                        isSelectedItem={props.isSelectedItem}
                        addToSelected={props.addToSelected}
                        addCurrentProductToCart={props.addCurrentProductToCart}
                        setIsShowModal={props.setIsShowModal}
                        isShowModal={props.isShowModal}
                        image={item.image}
                        name={item.name}
                        color={item.color}
                        cost={item.cost}
                        article={item.article}
                        productId={item.id}
                    />
                </li>
            ))}
        </ul>
    );
}

export default ListOfProducts;
