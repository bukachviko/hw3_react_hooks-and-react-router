import Button from "../Button/Button";
import "./Modal.css";
import PropTypes from "prop-types";

export default function Modal({functionSuccess, backgroundColor, header, text: mainText, nameBtn1, nameBtn2, toggleModal, classNameBtn}) {

    return (

        <div className="modal">
            <div onClick={toggleModal}
                 className="overlay"></div>
            <div className="modal-content" style={backgroundColor}>
                <div className="header__modal">
                    <h1 className="header__modal-title">{header}</h1>
                    <Button
                        btnClass="modal__headerCloseBtn"
                        myClick={toggleModal}
                        text={'Х'}
                    />
                </div>
                <p className="modal__text">{mainText}</p>
                <div className="modal__footer">
                    <Button
                        btnClass={classNameBtn}
                        text={nameBtn1}
                        myClick={functionSuccess}
                    />
                    <Button
                        text={nameBtn2}
                        myClick= {toggleModal}
                    />
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    addToCart: PropTypes.func,
    header: PropTypes.string,
    text: PropTypes.string,
    boolean: PropTypes.bool,
    nameBtn1: PropTypes.string,
    nameBtn2: PropTypes.string,
    toggleModal: PropTypes.func
}
