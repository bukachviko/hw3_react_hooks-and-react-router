import Button from "../Button/Button";
import {useState} from "react";
import { HiStar} from "react-icons/hi2";
import { HiOutlineStar } from "react-icons/hi2";
import "./ProductCard.css";
import PropTypes from "prop-types";

function ProductCard(props) {
    const [isSelected, setIsSelected] = useState(props.isSelectedItem(props.productId));

    return (
        <>
            <div>

                <img className="card__image-product" width="150" height="150" src={props.image} alt=""/>
                <h3 className="card__title-product">{props.name}</h3>
                <p className="card__color-product">{props.color}</p>
                <p className="card__price-product"> ${props.cost}</p>
                <p className="card__article-product">{props.article}</p>
                <div className="actions">
                    <Button
                        key={1}
                        btnClass="btn__add-to-cart"
                        backgroundColor={'#833ab4'}
                        text={"Add to cart"}
                        myClick={
                            () => {
                                props.addCurrentProductToCart(props.productId)
                                props.setIsShowModal()
                            }
                        }
                    />
                    <div className="actions actions__selected"
                         onClick={() => {
                             props.addToSelected({id: props.productId, name: props.name, image:props.image})
                             setIsSelected(props.isSelectedItem(props.productId))
                         }
                         }>
                        {isSelected ? <HiStar fill='rgba(131,58,180,1)'/> : <HiOutlineStar />}
                    </div>
                </div>

            </div>
        </>
    );
}

ProductCard.propTypes= {
    isSelectedItem: PropTypes.func,
    image: PropTypes.string,
    name: PropTypes.string,
    productId: PropTypes.number.isRequired
}

export default ProductCard;