import React, {useEffect, useState} from 'react';
import ListOfProducts from "../ListOfproducts/ListOfProducts";

const GetProducts = (props) => {

    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);


    useEffect(() => {
        fetch("/db.json")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    props.initItems(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])
    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return <ListOfProducts
            items={props.items}
            setIsShowModal={props.setIsShowModal}
            addToSelected={props.addToSelected}
            isSelectedItem={props.isSelectedItem}
            addCurrentProductToCart={props.addCurrentProductToCart}
        />
    }
};
export default GetProducts;