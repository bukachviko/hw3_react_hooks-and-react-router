import React from 'react';
import Cart from "../Cart/Cart";
import Selected from "../Selected/Selected";
import "./Header.css";
import { Link } from "react-router-dom";


function Header({countCartItems, countSelectedItems}) {
    return (
        <div className="header">
            <nav className="navbar">
                <h1>
                    <Link to="/" className="navbar__title">
                        Organic fruit store
                    </Link>
                </h1>
                <div className="header-links">
                    <ul>
                        <li>
                            <Link to="/selected">
                                <Selected countSelectedItems={countSelectedItems}/>
                            </Link>
                        </li>
                        <li>
                            <Link to="/cart">
                                <Cart countCartItems={countCartItems}/>
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
}

export default Header;
