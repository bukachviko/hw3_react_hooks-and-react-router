import React, { useState } from "react";
import './App.css';
import Header from "./Components/Header/Header";
import Modal from "./Components/Modal/Modal";
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import {Homepage} from "./Components/Pages/Homepage/Homepage";
import {Cartpage} from "./Components/Pages/Cartpage/Cartpage";
import {Selectedpage} from "./Components/Pages/Selectedpage/Selectedpage";


let cartItems = JSON.parse(localStorage.getItem('cart'));
if(cartItems === null) {
    cartItems = [];
}

let selectedItems = JSON.parse(localStorage.getItem('selected'));
if (selectedItems === null) {
    selectedItems = [];
}

function App() {
    const [countCartItems, updateCountCartItems] = useState(cartItems.length);
    const [items, setItems] = useState([]);
    const [countSelectedItems, updateCountSelectedItems] = useState(selectedItems.length);
    const [isShowModal, setIsShowModal] = useState(false );
    const [currentProductIdToCart, setCurrentProductIdToCart] = useState(0);

    const addCurrentProductToCart = (id) => {
        setCurrentProductIdToCart(id)
    }

    const initItems = (products) => {
        setItems(products)
    }

    const toggleModal = () => {
        setIsShowModal(!isShowModal)
    }

    const addToCart = () => {
        for(let product of items) {
            if (Number(product.id) === Number(currentProductIdToCart)) {
                cartItems.push(product)

                localStorage.setItem('cart', JSON.stringify(cartItems))

                updateCountCartItems(cartItems.length)
            }
        }
    }

    const deleteFromCart = (id) => {
        for(let key in cartItems) {
            if (Number(cartItems[key].id) === Number(id)) {
                cartItems.splice(key, 1)

                localStorage.setItem('cart', JSON.stringify(cartItems))

                updateCountCartItems(cartItems.length)
                break;
            }
        }
    }


    const addToSelected = (newItem) => {
        console.log(newItem)
        console.log(selectedItems)
    let existItem = selectedItems.filter((item) => item.id === newItem.id)
        console.log(existItem)
        if(existItem.length) {
            selectedItems = selectedItems.filter((item) => item.id !== newItem.id)
        } else {
            selectedItems.push(newItem)
        }

        localStorage.setItem('selected', JSON.stringify(selectedItems))

        updateCountSelectedItems(selectedItems.length)
    }

    const isSelectedItem = (id) => {
        return selectedItems.filter((item) => item.id === id).length > 0
    }

    return(

        <div className="wrapper">
            <Router>
                <Header countCartItems={countCartItems} countSelectedItems={countSelectedItems} />

                <Routes>
                    <Route index path="/" element={<Homepage
                        setIsShowModal={toggleModal}
                        items={items}
                        initItems={initItems}
                        addToSelected={addToSelected}
                        isSelectedItem={isSelectedItem}
                        addCurrentProductToCart={addCurrentProductToCart}
                    />}/>
                    <Route path="/selected" element={<Selectedpage items={selectedItems} addToSelected={addToSelected}/>}/>
                    <Route path="/cart" element={<Cartpage items={cartItems} deleteFromCart={deleteFromCart}  />}/>

                </Routes>
            </Router>

            {isShowModal &&  <Modal
                btnClass={"add__product-bnt"}
                backgroundColor={{backgroundColor: '#833ab4'}}
                header={"Додати товар в кошик?"}
                text={"Можливе замовлення від 1 шт"}
                boolean={true}
                nameBtn1={'Добре!'}
                nameBtn2={'Ні!'}
                functionSuccess={() => {
                    toggleModal()
                    addToCart()
                }}
                toggleModal={toggleModal}
            />}
        </div>
    )
}

export default App;
